"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = 'your_lab_partner.dashboard.CustomIndexDashboard'
"""

from django.utils.translation import gettext_lazy as _

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """

    def init_with_context(self, context):
        site_name = get_admin_site_name(context)

        self.children.append(modules.ModelList(
            title='Methods',
            column=1,
            models=('apps.core.models.Experiment', 'apps.core.models.ExperimentStage', 'apps.core.models.StageStep')
        ))

        # self.children.append(modules.ModelList(
        #     title='Formulas',
        #     column=1,
        #     models=('apps.core.models.Formula', 'apps.core.models.FormulaParameter',
        #             'apps.core.models.PhysicalUnit', 'apps.core.models.Measure')
        # ))

        self.children.append(modules.ModelList(
            title='Feedback & Requests',
            column=1,
            models=('apps.core.models.Feedback', )
        ))

        self.children.append(modules.ModelList(
            title='Users',
            column=1,
            models=('apps.user.models.CustomUser', 'apps.notifications.models.CustomFCMDevice')
        ))

        self.children.append(modules.ModelList(
            title='Notifications & Modal Windows',
            column=1,
            models=('apps.notifications.models.ReminderPeriodicPush',
                    'apps.notifications.models.ModalWindow', 'apps.notifications.models.NewsPush')
        ))

        # append a recent actions module
        self.children.append(modules.RecentActions(
            _('Recent actions'),
            limit=5,
            collapsible=False,
            column=3,
        ))

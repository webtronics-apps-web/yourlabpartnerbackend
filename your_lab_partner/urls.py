"""your_lab_partner URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from django.conf.urls.static import static
from django.contrib.auth import get_user_model
from rest_framework.routers import DefaultRouter
from apps.notifications.views import EventsApiViewSet
from rest_framework_simplejwt import views as simple_jwt_views
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView, SpectacularRedocView
from apps.user.views import (CustomTokenObtainPairView, reset_user_password,
                             ActivateUser, privacyPolicy, CustomUserViewSet)

users_router = DefaultRouter()
events_router = DefaultRouter()
users_router.register("users", CustomUserViewSet)
events_router.register("events", EventsApiViewSet, basename="EventsRaising")

User = get_user_model()

urlpatterns = [
    # admin
    path('grappelli/', include('grappelli.urls')),  # grappelli URLS
    path('_nested_admin/', include('nested_admin.urls')),  # nested inlines
    path('admin/', admin.site.urls),  # admin site

    # authorization
    path("auth/", include(users_router.urls)),
    path('auth/jwt/create/', CustomTokenObtainPairView.as_view(),
         name='token_obtain_pair'),
    path("auth/jwt/refresh/",
         simple_jwt_views.TokenRefreshView.as_view(), name="jwt-refresh"),
    path("auth/jwt/verify/",
         simple_jwt_views.TokenVerifyView.as_view(), name="jwt-verify"),
    path('auth/activate_user/<str:uid>/<str:token>/',
         ActivateUser.as_view({'get': 'activation'}), name='activation'),
    path('auth/password/reset/<str:uid>/<str:token>/',
         reset_user_password, name='reset_user_password'),

    # privacy policy
    path('privacy-policy', privacyPolicy, name='privacyPolicy'),

    # api
    path('api/v1/', include('apps.api.urls')),
    path("api/v1/", include(events_router.urls)),

    # # OpenAPI 3 documentation with Swagger UI
    path("api/schema/", SpectacularAPIView.as_view(), name="schema"),
    path("api/swagger/", SpectacularSwaggerView.as_view(template_name="swagger-ui.html",
                                                        url_name="schema"), name="swagger-ui",),
    path('api/docs/', SpectacularRedocView.as_view(url_name='schema'), name='redoc'),

]

admin.site.site_url = ''

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)

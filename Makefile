.PHONY: init build rebuild migrate 

init:
	curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python
	poetry install
build:
	docker-compose build
rebuild:
	docker-compose build --force-rm --no-cache
migrate:
	docker-compose run --rm web python manage.py migrate

load_base_fixtures:
	python manage.py loaddata measures
	python manage.py loaddata units
	python manage.py loaddata formula_parameters
	python manage.py loaddata formulas

load_base_experiments:
	python manage.py loaddata initial_methods
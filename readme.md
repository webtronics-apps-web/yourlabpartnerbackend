# Дамп БД:

0. Получаем имя контейнера с джангой

```
docker ps
```

1. Входим в контейнер

```
docker exec -t -i <CONTAINER_NAME> bash
```

2. Создаем фикстуры (общий вид, при необходимости можно эксклудить-инклудить нужные таблицы)

```
python manage.py dumpdata core > coredata.json
```

3. Выходим из консоли контейнера
4. Копируем файл из контейнера в корень репы

```
docker cp <CONTAINER_NAME>:usr/src/app/coredata.json /
```

### Настройка Dokku для авто деплоя

0. В системе должен быть SSH ключ, совпадающий с ключем на сервере

1. Добавляем удаленный репозиторий

```
git remote add dokku dokku@13.36.46.176:backend
```

2. Пушим - дальше докку все делает сам

```
git push dokku master
```

# На сервере

## Backend

1. Устанавливаем .ssh ключи
2. Dokku install:

```
wget https://raw.githubusercontent.com/dokku/dokku/v0.26.8/bootstrap.sh;
sudo DOKKU_TAG=v0.26.8 bash bootstrap.sh
```

3. Создаем приложение
   dokku apps:create backend
4. Выполняем следующие команды для установки необходимых плагинов

Чтобы докку увидел докерфайл в подпапке проекта

```
sudo dokku plugin:install https://github.com/mimischi/dokku-dockerfile.git
```

```
Установка Postgres plugin + создание контейнера для БД
sudo dokku plugin:install https://github.com/dokku/dokku-postgres.git postgres
dokku postgres:create backend_pg
dokku postgres:expose backend_pg
dokku postgres:info backend_pg
dokku postgres:link backend_pg backend
```

5. Устанавливаем конфиги

```
dokku config:set --no-restart backend DEBUG=False
dokku config:set --no-restart backend DJANGO_ALLOWED_HOSTS=["yourlabpartneradmin.co.uk"]
dokku config:set --no-restart backend DJANGO_SECRET_KEY=$(echo `openssl rand -base64 100` | tr -d \=+ | cut -c 1-64)
dokku config:set --no-restart backend EMAIL_HOST="smtp.gmail.com"
dokku config:set --no-restart backend EMAIL_PORT=465
dokku config:set --no-restart backend EMAIL_HOST_USER="info@yourlabpartner.co.uk"
dokku config:set --no-restart backend EMAIL_HOST_PASSWORD="ydimdkrtogaklqlu"
dokku config:set --no-restart backend EMAIL_USE_SSL="True"
dokku config:set --no-restart backend DEFAULT_FROM_EMAIL="info@yourlabpartner.co.uk"
dokku config:set --no-restart backend DOKKU_LETSENCRYPT_EMAIL=kedovnet@gmail.com
dokku nginx:set backend client-max-body-size 30m
```

Плагин `dokku-dockerfile` должен быть установлен!!

```
dokku dockerfile:set backend docker/dokku/Dockerfile
```

6. Устанавливаем SSL

```
sudo dokku plugin:install https://github.com/dokku/dokku-letsencrypt.git
dokku letsencrypt:enable backend
```
7. Папка для хранения изображений
```
dokku storage:ensure-directory notes_images --chown false
dokku storage:mount backend /var/lib/dokku/data/storage/notes_images:/app/media
```

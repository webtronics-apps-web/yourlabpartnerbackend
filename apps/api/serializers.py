from collections import OrderedDict
from rest_framework import serializers

from apps.core.models import (ActiveExperiment, ExperimentStage, Feedback, Formula, FormulaParameter,
                              NoteImage, Note, Measure, PhysicalUnit, Experiment, StageStep)


class RecursiveSerializer(serializers.Serializer):
    """ Рекурсивный вывод children"""

    def to_representation(self, value):
        serializer = self.parent.parent.__class__(value, context=self.context)
        return serializer.data


class FilterOnlyParentsListSerializer(serializers.ListSerializer):
    """ Фильтр List - вывод только parents"""

    def to_representation(self, data):
        data = data.filter(parent=None)
        return super().to_representation(data)


class FeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feedback
        fields = ('type', 'name', 'message', 'source')


class NoteImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = NoteImage
        fields = ('id', 'image', 'note')


class NoteDetailSerializer(serializers.ModelSerializer):
    note_images = NoteImageSerializer(many=True)

    class Meta:
        model = Note
        fields = '__all__'


class NoteCreateSerializer(serializers.ModelSerializer):
    note_images = NoteImageSerializer(many=True, required=False)

    class Meta:
        model = Note
        fields = ('pk', "name", "content", "tags",
                  "is_favourite", 'note_images')
        read_only_fields = ["pk", 'note_images']


class NoteListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Note
        fields = ('pk', 'name', 'content_preview', "is_favourite", 'tags',)


class MeasureSerializer(serializers.ModelSerializer):
    children = RecursiveSerializer(many=True)
    multiplier_to_main = serializers.CharField(
        source='string_multiplier_to_main')

    def to_representation(self, instance):
        result = super(MeasureSerializer, self).to_representation(instance)
        return OrderedDict([(key, result[key]) for key in result if result[key] is not None])

    class Meta:
        list_serializer_class = FilterOnlyParentsListSerializer
        model = Measure
        fields = ('pk', 'name', 'symbol', 'multiplier_to_main', 'children')


class PhysicalUnitSerializer(serializers.ModelSerializer):
    measures = MeasureSerializer(many=False, source="default_measure")

    class Meta:
        model = PhysicalUnit
        fields = ('pk', 'name', 'symbol', 'measures')


class FormulaParameterSerializer(serializers.ModelSerializer):
    # default_measure = MeasureSerializer(many=False)
    physical_unit = PhysicalUnitSerializer(many=False)
    name = serializers.CharField(source='description')

    class Meta:
        model = FormulaParameter
        fields = ('pk', 'name', 'description',
                  'physical_unit', 'default_measure')


class FormulaListSerializer(serializers.ModelSerializer):
    is_favourite = serializers.SerializerMethodField()

    class Meta:
        model = Formula
        fields = ('pk', 'name', 'description', 'is_favourite', 'is_popular')

    def get_is_favourite(self, instance):
        return self.context.get('request').user.favourite_formulas.filter(pk=instance.pk).exists()


class FormulaRelatedSerializer(serializers.ModelSerializer):
    parameters = FormulaParameterSerializer(many=True)
    is_favourite = serializers.SerializerMethodField()

    class Meta:
        model = Formula
        fields = ('pk', 'name', 'description', 'parameters', 'expression', 'calculation_expression', 'is_favourite',
                  'views_counter', 'is_popular')

    def get_is_favourite(self, instance):
        return self.context.get('request').user.favourite_formulas.filter(pk=instance.pk).exists()


class FormulaRetrieveSerializer(serializers.ModelSerializer):
    parameters = FormulaParameterSerializer(many=True)
    related = FormulaRelatedSerializer(many=True)
    is_favourite = serializers.SerializerMethodField()

    class Meta:
        model = Formula
        fields = ('pk', 'name', 'description', 'parameters', 'expression', 'calculation_expression', 'is_favourite',
                  'views_counter', 'is_popular', 'related')

    def get_is_favourite(self, instance):
        return self.context.get('request').user.favourite_formulas.filter(pk=instance.pk).exists()


class FormulaStageStepSerializer(serializers.ModelSerializer):
    parameters = FormulaParameterSerializer(many=True)
    is_favourite = serializers.SerializerMethodField()

    class Meta:
        model = Formula
        fields = ('pk', 'name', 'description', 'parameters', 'expression', 'is_favourite',
                  'views_counter', 'is_popular')

    def get_is_favourite(self, instance):
        return self.context.get('request').user.favourite_formulas.filter(pk=instance.pk).exists()


class StageStepSerializer(serializers.ModelSerializer):
    formula = FormulaStageStepSerializer(many=False)
    timer_default_duration = serializers.SerializerMethodField()

    def to_representation(self, obj):
        data = super(StageStepSerializer, self).to_representation(obj)
        if not obj.can_create_timer:
            data.pop('timer_default_duration')
        return data

    def get_timer_default_duration(self, obj):
        return obj.timer_default_duration.total_seconds() if obj.timer_default_duration else 0

    class Meta:
        model = StageStep
        fields = ('pk', 'description', 'help_text', 'can_create_note', 'can_create_timer',
                  'timer_default_duration', 'formula')


class ExperimentStageSerializer(serializers.ModelSerializer):
    steps = StageStepSerializer(many=True)

    class Meta:
        model = ExperimentStage
        fields = ('pk', 'name', 'description', 'position', 'steps')


class CurrentStageAndStepSerializer(serializers.ModelSerializer):
    is_experiment_active = serializers.SerializerMethodField()

    def get_is_experiment_active(self, instance):
        return True

    class Meta:
        model = ActiveExperiment
        fields = ('is_experiment_active', 'stage', 'step')


class ExperimentListSerializer(serializers.ModelSerializer):
    is_favourite = serializers.SerializerMethodField()
    active = serializers.SerializerMethodField()

    # ПОТОМ УДАЛИТЬ
    is_active = serializers.SerializerMethodField()

    class Meta:
        model = Experiment
        fields = ('pk', 'name', 'company', 'description', 'active',
                  'is_favourite', 'is_popular', 'is_active')

    def get_is_favourite(self, instance):
        return self.context.get('request').user.favourite_experiments.filter(pk=instance.pk).exists()

    def get_is_active(self, instance):
        user = self.context.get('request').user
        return ActiveExperiment.objects.filter(experiment=instance.pk, user=user).exists()

    def get_active(self, instance):
        user = self.context.get('request').user
        current = ActiveExperiment.objects.filter(
            experiment=instance.pk, user=user).first()
        return CurrentStageAndStepSerializer(current).data

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if not data['company']:
            data['company'] = ""
        if 'is_experiment_active' not in data['active']:
            data['active']['is_experiment_active'] = False
        return data


class ExperimentDetailSerializer(serializers.ModelSerializer):
    stages = ExperimentStageSerializer(many=True)
    is_favourite = serializers.SerializerMethodField()
    active = serializers.SerializerMethodField()

    # ПОТОМ УДАЛИТЬ
    is_active = serializers.SerializerMethodField()

    class Meta:
        model = Experiment
        fields = ('pk', 'name', 'company', 'description', 'active',
                  'is_favourite', 'is_popular', 'is_active', 'stages')

    def get_is_favourite(self, instance):
        return self.context.get('request').user.favourite_experiments.filter(pk=instance.pk).exists()

    def get_active(self, instance):
        user = self.context.get('request').user
        current = ActiveExperiment.objects.filter(
            experiment=instance.pk, user=user).first()
        return CurrentStageAndStepSerializer(current).data

    def get_is_active(self, instance):
        user = self.context.get('request').user
        return ActiveExperiment.objects.filter(experiment=instance.pk, user=user).exists()

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if not data['company']:
            data['company'] = ""
        if 'is_experiment_active' not in data['active']:
            data['active']['is_experiment_active'] = False
        return data

from rest_framework.decorators import action
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend

from drf_spectacular.utils import extend_schema_view, extend_schema, OpenApiParameter

from rest_framework import viewsets
from rest_framework import generics, permissions
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response


from apps.api.permissions import IsAuthor

from apps.api.serializers import (NoteListSerializer, NoteCreateSerializer, ExperimentDetailSerializer,
                                  NoteDetailSerializer, NoteImageSerializer, FeedbackSerializer,
                                  MeasureSerializer, PhysicalUnitSerializer, FormulaParameterSerializer,
                                  FormulaListSerializer, FormulaRetrieveSerializer, ExperimentListSerializer)

from apps.core.models import (Note, NoteImage, Feedback, Measure, PhysicalUnit,
                              FormulaParameter, Formula, Experiment, ActiveExperiment,
                              ExperimentStage, StageStep)


@ extend_schema_view(
    list=extend_schema(description='Возвращает список заметок АВТОРИЗОВАННОГО пользователя с возможностью'
                                   ' поиска и фильтации. Поиск ищет по полям name, tags и content,'
                                   ' фильтрация доступна по полю is_favourite , сортировка возможна по следующим полям'
                                   ' (можно через запятую) -  "name", "created_at", "updated_at", "is_favourite" ,'
                                   ' для фильтрации DESC использовать "-" перед полем, например "-name"'),
    retrieve=extend_schema(description='Возвращает заметку по ID'),
    create=extend_schema(
        description='Создает заметку на авторизованного пользователя'),
    set_favourite=extend_schema(
        description='Устанавливает "is_favourite" в true'),
    unset_favourite=extend_schema(
        description='Устанавливает "is_favourite" в false'),
)
class NoteViewSet(viewsets.ModelViewSet):
    queryset = Note.objects.all()
    ordering = ('-created_at', )
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)
    search_fields = ('name', 'tags', 'content')
    filter_fields = ('is_favourite',)
    ordering_fields = ['name', 'created_at', 'updated_at', 'is_favourite']
    permission_classes = [IsAuthor]

    def get_serializer_class(self):
        if self.action == "list":
            return NoteListSerializer
        elif self.action == "retrieve":
            return NoteDetailSerializer
        else:
            return NoteCreateSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    @ action(detail=True, methods=['get'])
    def set_favourite(self, request, pk=None):
        instance = get_object_or_404(Note, pk=pk)
        if instance.user != request.user:
            return Response(data={"error": "Доступ запрещен"}, status=403)
        instance.is_favourite = True
        instance.save()
        serializer = NoteDetailSerializer(instance)
        return Response(serializer.data)

    @ action(detail=True, methods=['get'])
    def unset_favourite(self, request, pk=None):
        instance = get_object_or_404(Note, pk=pk)
        if instance.user != request.user:
            return Response(data={"error": "Доступ запрещен"}, status=403)
        instance.is_favourite = False
        instance.save()
        serializer = NoteDetailSerializer(instance)
        return Response(serializer.data)

    def get_queryset(self):
        return Note.objects.filter(user=self.request.user)


@ extend_schema_view(
    list=extend_schema(description='Возвращает список изображений'),
    retrieve=extend_schema(description='Возвращает изображение по ID'),
)
class NoteImageViewSet(viewsets.ModelViewSet):
    queryset = NoteImage.objects.all()
    serializer_class = NoteImageSerializer
    permission_classes = [IsAuthor]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        return NoteImage.objects.filter(user=self.request.user)


@ extend_schema_view(
    post=extend_schema(description='Создает тикет указанного типа ("FRML"-"FORMULAE", "EXPR"-"Experiment(method),'
                                   '"MISC"-"default value). При необходимости указать source (для эксперимента?).'
                                   'Тикет автоматически создается от имени авторизованного пользователя!'),
)
class FeedbackCreateView(generics.CreateAPIView):
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return Response({
            'status': 200,
            'message': 'Feedback was sent successfully',
            'data': response.data
        })


class MeasureViewSet(viewsets.ReadOnlyModelViewSet):
    """Меры"""
    queryset = Measure.objects.all()
    serializer_class = MeasureSerializer
    pagination_class = None


class PhysicalUnitViewSet(viewsets.ReadOnlyModelViewSet):
    """Получение физических величин"""
    queryset = PhysicalUnit.objects.all()
    serializer_class = PhysicalUnitSerializer


class FormulaParameterViewSet(viewsets.ReadOnlyModelViewSet):
    """Получение параметра формулы"""
    queryset = FormulaParameter.objects.all()
    serializer_class = FormulaParameterSerializer


@ extend_schema_view(
    list=extend_schema(description='Список формул'),
    retrieve=extend_schema(description='Получение формулы по ID'),
    get_favourite=extend_schema(
        description='Возвращет список любимых формул пользователя'),
    set_favourite=extend_schema(
        description='Добавляет формулу "любимые" формулы'),
    unset_favourite=extend_schema(
        description='Убирает формулу из "любимых" формул'),

)
class FormulaViewSet(viewsets.ReadOnlyModelViewSet):
    """Формулы"""
    queryset = Formula.objects.all()
    serializer_class = FormulaListSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)
    filter_fields = ('is_popular', )
    ordering_fields = ['name', 'views_counter', ]
    permission_classes = [permissions.IsAuthenticated]
    search_fields = ('name',)

    @ action(detail=False, methods=['get'])
    def get_favourite(self, request):
        queryset = request.user.favourite_formulas
        queryset = self.filter_queryset(queryset)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = FormulaListSerializer(
                page, many=True, context={'request': request})
            return self.get_paginated_response(serializer.data)

        serializer = FormulaListSerializer(
            queryset, many=True, context={'request': request})
        return Response(serializer.data)

    @ action(detail=True, methods=['get'])
    def set_favourite(self, request, pk=None):
        instance = get_object_or_404(Formula, pk=pk)
        request.user.favourite_formulas.add(instance)
        serializer = FormulaRetrieveSerializer(
            instance, context={'request': request})
        return Response(serializer.data)

    @ action(detail=True, methods=['get'])
    def unset_favourite(self, request, pk=None):
        instance = get_object_or_404(Formula, pk=pk)
        request.user.favourite_formulas.remove(instance)
        serializer = FormulaRetrieveSerializer(
            instance, context={'request': request})
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        instance = get_object_or_404(Formula, pk=pk)
        instance.views_counter += 1
        instance.save()
        serializer = FormulaRetrieveSerializer(
            instance, context={'request': request})
        return Response(serializer.data)


@ extend_schema_view(
    list=extend_schema(description='Список экспериментов ("методов")'),
    retrieve=extend_schema(description='Получение эксперимента по ID'),
    get_favourite=extend_schema(
        description='Возвращет список любимых экспериментов пользователя'),
    set_favourite=extend_schema(
        description='Добавляет эксперимент "любимые" эксперименты'),
    unset_favourite=extend_schema(
        description='Убирает эксперимент из "любимых" экспериментов'),
    get_active=extend_schema(
        description='Возвращает список активных экспериментов пользователя'),
    set_active=extend_schema(
        parameters=[
            OpenApiParameter(name='stageId', location=OpenApiParameter.QUERY,
                             description='Optional active stage ID', required=False, type=int),
            OpenApiParameter(name='stepId', location=OpenApiParameter.QUERY,
                             description='Optional active step ID', required=False, type=int),
        ],
        description='Добавляет эксперимент список активных экспериментов пользователя'),
    unset_active=extend_schema(
        description='Убирает эксперимент из списка активных экспериментов пользователя'),

)
class ExperimentViewSet(viewsets.ReadOnlyModelViewSet):
    """Эксперименты ("методы")"""
    queryset = Experiment.objects.filter(is_published=True)
    serializer_class = ExperimentListSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)
    filter_fields = ('is_popular', )
    ordering_fields = ['name', 'created_at', 'updated_at']
    permission_classes = [permissions.IsAuthenticated]
    search_fields = ('name',)

    @ action(detail=False, methods=['get'])
    def get_favourite(self, request):
        queryset = request.user.favourite_experiments.filter(is_published=True)
        queryset = self.filter_queryset(queryset)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = ExperimentListSerializer(
                page, many=True, context={'request': request})
            return self.get_paginated_response(serializer.data)

        serializer = ExperimentListSerializer(
            queryset, many=True, context={'request': request})
        return Response(serializer.data)

    @ action(detail=True, methods=['get'])
    def set_favourite(self, request, pk=None):
        instance = get_object_or_404(Experiment, pk=pk, is_published=True)
        request.user.favourite_experiments.add(instance)
        serializer = ExperimentListSerializer(
            instance, context={'request': request})
        return Response(serializer.data)

    @ action(detail=True, methods=['get'])
    def unset_favourite(self, request, pk=None):
        instance = get_object_or_404(Experiment, pk=pk, is_published=True)
        request.user.favourite_experiments.remove(instance)
        serializer = ExperimentListSerializer(
            instance, context={'request': request})
        return Response(serializer.data)

    @ action(detail=False, methods=['get'])
    def get_active(self, request):
        user = request.user
        # active_experiments = ActiveExperiment.objects.filter(user=user).
        queryset = Experiment.objects.filter(
            active_experiments__user=user, is_published=True)
        queryset = self.filter_queryset(queryset)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = ExperimentListSerializer(
                page, many=True, context={'request': request})
            return self.get_paginated_response(serializer.data)

        serializer = ExperimentListSerializer(
            queryset, many=True, context={'request': request})
        return Response(serializer.data)

    @ action(detail=True, methods=['get'])
    def set_active(self, request, pk=None):
        params = request.query_params
        stage_id = params['stageId'] if 'stageId' in params else None
        step_id = params['stepId'] if 'stepId' in params else None

        experimentInstance = get_object_or_404(
            Experiment, pk=pk, is_published=True)

        activeExperiment, is_created = ActiveExperiment.objects.update_or_create(
            user=request.user, experiment=experimentInstance, defaults={'stage': None, 'step': None})

        if stage_id:
            stage = get_object_or_404(ExperimentStage, pk=stage_id)
            activeExperiment.stage = stage

        if step_id:
            step = get_object_or_404(StageStep, pk=step_id)
            activeExperiment.step = step

        activeExperiment.save()

        serializer = ExperimentDetailSerializer(
            experimentInstance, context={'request': request})
        return Response(serializer.data)

    @ action(detail=True, methods=['get'])
    def unset_active(self, request, pk=None):
        activeExperiment = get_object_or_404(
            ActiveExperiment, experiment=pk, user=request.user)
        experiment = activeExperiment.experiment
        activeExperiment.delete()
        serializer = ExperimentDetailSerializer(
            experiment, context={'request': request})
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        instance = get_object_or_404(Experiment, pk=pk, is_published=True)
        serializer = ExperimentDetailSerializer(
            instance, context={'request': request})
        return Response(serializer.data)

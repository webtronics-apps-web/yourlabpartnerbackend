from django.contrib import admin
from django.contrib.auth import get_user_model
from .models import CustomUser
from .forms import CustomUserAdminForm

UserModel = get_user_model()


@admin.register(CustomUser)
class CustomUserAdmin(admin.ModelAdmin):
    list_display = ['pk', 'username', 'email', 'date_joined', 'last_activity',
                    'saved_notes_counter', 'calculated_formulas_counter',
                    'timer_finished_counter']
    list_display_links = ['pk', 'username', 'email', 'date_joined', 'last_activity']
    list_filter = ('email', 'username', 'id')
    search_fields = ['username', 'email']
    form = CustomUserAdminForm
    autocomplete_fields = ['favourite_formulas', 'favourite_experiments']


from django.contrib.auth import get_user_model


UserModel = get_user_model()


class UpdateLastActivityMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.is_authenticated:
            user = UserModel.objects.get(pk=request.user.id)
            user.update_last_activity()
        response = self.get_response(request)
        return response

from django import forms

from .models import CustomUser


class CustomUserAdminForm(forms.ModelForm):

    class Meta:
        model = CustomUser
        fields = ('username', 'email', 'is_active', 'is_superuser', 'is_staff', 'last_activity',
                  'favourite_formulas', 'favourite_experiments', 'is_show_rate_modal',
                  'is_rated_app', 'saved_notes_counter', 'calculated_formulas_counter',
                  'timer_finished_counter')

from rest_framework_simplejwt.views import TokenViewBase
from rest_framework_simplejwt.exceptions import AuthenticationFailed, InvalidToken, TokenError
from .serializers import CustomTokenObtainPairSerializer, InActiveUser
from rest_framework import status
from djoser.conf import settings
from django.shortcuts import render

from django.contrib import messages
from django.http import HttpResponseRedirect

from rest_framework.decorators import (api_view, permission_classes,
                                       renderer_classes)
from rest_framework.permissions import AllowAny
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer


from djoser.views import UserViewSet
from rest_framework.response import Response


class CustomUserViewSet(UserViewSet):
    def get_serializer_class(self):
        if self.action == "reset_password":
            return settings.SERIALIZERS.custom_password_reset
        return super().get_serializer_class()


class ActivateUser(UserViewSet):
    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        kwargs.setdefault('context', self.get_serializer_context())
        tmpObj = kwargs['data'].copy()
        # this line is the only change from the base implementation.
        tmpObj["uid"] = self.kwargs['uid']
        tmpObj["token"] = self.kwargs['token']
        kwargs['data'] = tmpObj

        return serializer_class(*args, **kwargs)

    def activation(self, request, uid, token, *args, **kwargs):
        try:
            super().activation(request, *args, **kwargs)
            return render(request, 'custom_response.html',
                          context={'custom_messages': ['Account has been successfully activated']})
        except Exception as e:
            return render(request, 'custom_response.html',
                          context={'custom_messages': [f'Activation error: {str(e)}']})

    def reset_password_confirm(self, request, *args, **kwargs):
        try:
            response = super().reset_password_confirm(request, *args, **kwargs)
            if response.status_code == 204:
                # Give some feedback to the user.
                messages.success(request,
                                 'Your password has been changed successfully!')
                return render(request, 'custom_response.html',
                              context={'custom_messages': ['Your password has been changed successfully']})
            else:
                try:
                    response_object = response.json()
                    response_object_keys = response_object.keys()
                    for key in response_object_keys:
                        decoded_string = response_object.get(
                            key)[0].replace("'", "\'")
                        messages.error(request, f'{decoded_string}')
                except Exception:
                    messages.error(request, f'{response.text}')
                finally:
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

        except Exception as e:
            errors = e.get_full_details()
            for field, error in errors.items():
                if error[0]['code'] == 'required':
                    message = 'Fill in the required fields!'
                    messages.error(request, message)
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
                else:
                    messages.error(request, error[0]['message'])
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@api_view(('GET', 'POST'))
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
@permission_classes([AllowAny])
def reset_user_password(request, **kwargs):
    if not request.POST:
        return render(request, 'user/reset_user_password.html')

    return ActivateUser.as_view({'post': 'reset_password_confirm'})(request._request, **kwargs)


def privacyPolicy(request):
    return render(request, 'privacy_policy.html')


class CustomTokenObtainPairView(TokenViewBase):
    """
    Takes a set of user credentials and returns an access and refresh JSON web
    token pair to prove the authentication of those credentials.

    Returns HTTP 424 when user is inactive and HTTP 401 when login credentials are invalid.
    """
    serializer_class = CustomTokenObtainPairSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
        except AuthenticationFailed:
            raise InActiveUser()
        except TokenError:
            raise InvalidToken()

        return Response(serializer.validated_data, status=status.HTTP_200_OK)

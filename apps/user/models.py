from django.db import models
from django.utils import timezone
from apps.notifications.models import EventsTypes, ModalWindow
from apps.user.managers import CustomUserManager
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _


class CustomUser(AbstractUser):
    """ Кастомная модель Пользователя"""
    username = models.CharField(_('Username'), max_length=255)
    email = models.EmailField(_('E-mail'), unique=True)
    last_activity = models.DateTimeField(verbose_name="Last activity", null=True, blank=True)
    favourite_formulas = models.ManyToManyField("core.Formula", verbose_name="Favourite user formulas",
                                                blank=True)
    favourite_experiments = models.ManyToManyField("core.Experiment", verbose_name="Favourite user methods",
                                                   blank=True, related_name="favourite_user_experiments")
    is_show_rate_modal = models.BooleanField(verbose_name="Is user allowed to show 'Rate app' modal window",
                                             default=True,
                                             help_text="Default value = True.  \
                                             Changes only if user pressed 'Do not show modal window again' button")

    is_rated_app = models.BooleanField(verbose_name="Did user opened store to rate app",
                                       default=False)

    saved_notes_counter = models.PositiveIntegerField(
        verbose_name="Saved notes count", default=0, help_text="How many times user saved notes")

    calculated_formulas_counter = models.PositiveIntegerField(verbose_name="Calculated formulas count", default=0)
    timer_finished_counter = models.PositiveIntegerField(verbose_name="Finished timers count", default=0)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    objects = CustomUserManager()

    def __str__(self):
        return self.email

    def update_last_activity(self):
        self.last_activity = timezone.now()
        self.save(update_fields=['last_activity'])

    def __get_event_counter(self, event_id):
        match event_id:
            case EventsTypes.ON_FIRST_FORMULA_RESULT:
                return self.calculated_formulas_counter
            case EventsTypes.ON_TIMER_FINISH:
                return self.timer_finished_counter
            case EventsTypes.ON_NOTE_SAVE:
                return self.saved_notes_counter

    def get_rate_modal(self, event_id):
        if not self.is_show_rate_modal:
            return None
        return ModalWindow.get_modal_to_show(self.__get_event_counter(event_id), event_id)

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')

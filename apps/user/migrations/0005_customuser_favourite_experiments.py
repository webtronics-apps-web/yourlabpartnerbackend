# Generated by Django 4.0 on 2022-01-15 16:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0022_remove_stagestep_experiment_and_more'),
        ('user', '0004_alter_customuser_favourite_formulas'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='favourite_experiments',
            field=models.ManyToManyField(blank=True, to='core.Experiment', verbose_name='Favourite user experiments'),
        ),
    ]

from django import forms
from django.contrib.admin.widgets import AutocompleteSelectMultiple
from django.contrib import admin

from apps.core.models import Experiment, Formula


class ExperimentAdminForm(forms.ModelForm):

    class Meta:
        model = Experiment
        fields = '__all__'


class FormulaAdminForm(forms.ModelForm):
    class Meta:
        model = Formula
        fields = "__all__"
        widgets = {
            'parameters': AutocompleteSelectMultiple(
                Formula._meta.get_field('parameters'),
                admin.site,
                # attrs={'data-dropdown-auto-width': 'true'}
                attrs={'style': 'width: 4000px'}
            ),
        }

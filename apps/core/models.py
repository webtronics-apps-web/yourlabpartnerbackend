from django.contrib import admin
from django.contrib.auth import get_user_model
from django.db import models
from rest_framework.exceptions import ValidationError

from . import mixins
from django.utils.translation import gettext_lazy as _
from django.contrib.postgres.fields import ArrayField
from django.utils import timezone
from django_resized import ResizedImageField

UserModel = get_user_model()


def get_feedback_type_choices():
    return [
        ('FRML', 'Formulae'),
        ('EXPR', 'Method'),
        ('MISC', 'Miscellaneous')]


def get_feedback_status_choices():
    return [
        ("UNRD", 'Unread'),
        ("PRCS", 'In process...'),
        ("SLVD", 'Solved'),
        ("OLD", 'Old')]


class Note(mixins.CreatedAtMixin, mixins.UpdatedAtMixin):
    """ Сущность заметки"""
    name = models.CharField(max_length=255, verbose_name=_('Note name'))
    content = models.TextField(verbose_name=_(
        'Note content'), null=True, blank=True)
    tags = ArrayField(models.CharField(max_length=100),
                      default=list, null=True, blank=True)
    is_favourite = models.BooleanField(
        default=False, verbose_name=_('Is favourite'))
    user = models.ForeignKey(UserModel, on_delete=models.CASCADE)

    @property
    def content_preview(self):
        return self.content[:100]

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['created_at']


class NoteImage(models.Model):
    """ Изображения в  заметке"""
    note = models.ForeignKey(
        Note, on_delete=models.CASCADE, related_name="note_images")
    image = ResizedImageField(
        size=[1920, 1080], quality=75, upload_to='note_images/', force_format='JPEG')
    user = models.ForeignKey(UserModel, on_delete=models.CASCADE)

    def __str__(self):
        return f'Image #{self.pk} (Note {self.note.name})'


class Feedback(mixins.CreatedAtMixin, mixins.UpdatedAtMixin):
    """ Запросы от пользователей, такие как "запрос метода" и "запрос формулы" """
    user = models.ForeignKey(
        UserModel, on_delete=models.CASCADE, verbose_name="E-mail")
    name = models.CharField(max_length=100, verbose_name=_('Name'))
    message = models.TextField(verbose_name=_('Feedback message'))
    source = models.CharField(max_length=255, verbose_name=_(
        'Source'), null=True, blank=True)
    type = models.CharField(max_length=4, choices=get_feedback_type_choices(), default='MISC',
                            verbose_name="Request`s category")
    status = models.CharField(
        max_length=4, choices=get_feedback_status_choices(), default='UNRD')

    @admin.display(boolean=None, ordering='user__username', description="Full Name")
    def username(self):
        return self.user.username

    @admin.display(boolean=None, ordering='message', description="Message preview")
    def message_preview(self):
        return self.message[:50]

    def __str__(self):
        return f'Feedback by {self.user} from {self.created_at}'


class Measure(models.Model):
    """ Единица измерения"""
    name = models.CharField(max_length=255, verbose_name=_('Measure name'))
    symbol = models.CharField(max_length=10, verbose_name=_('Measure symbol'))
    parent = models.ForeignKey("self", verbose_name="Main measure", on_delete=models.CASCADE,
                               null=True, blank=True, related_name="children")
    multiplier_to_main = models.FloatField(
        verbose_name="Multiplier to main measure", null=True, blank=True)

    def __str__(self):
        return f'{self.name}, {self.symbol}'

    @property
    def string_multiplier_to_main(self):
        return float_to_str(self.multiplier_to_main) if self.multiplier_to_main else "1"

    @staticmethod
    def convert_measures(old_measure, old_value, new_measure):
        """ Конвертация значений величин"""
        if not Measure.can_convert(old_measure, new_measure):
            raise ValidationError(
                detail="Невозожно сконвертировать указанные меры!")
        new_value = float(old_value)
        if old_measure.parent:  # Приводим к базовой мере
            new_value = old_measure.multiplier_to_main * old_value
        if new_measure.parent:  # если новая мера НЕ базовая
            new_value = new_value / new_measure.multiplier_to_main
        return new_value

    @staticmethod
    def can_convert(measure_1, measure_2):
        """ Проверка, можно ли сконвертировать значения"""
        parent_1 = measure_1.parent or measure_1
        parent_2 = measure_2.parent or measure_2
        if parent_1 != parent_2:
            return False
        return True


class PhysicalUnit(models.Model):
    """ Величина """
    name = models.CharField(max_length=255, verbose_name=_('Unit Name'))
    symbol = models.CharField(max_length=10, verbose_name=_('Unit Symbol'))
    default_measure = models.ForeignKey(
        Measure, verbose_name="Main measure", on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.name}, {self.symbol}'

    class Meta:
        verbose_name = "Unit"


class FormulaParameter(models.Model):
    """ Параметр формулы """
    description = models.CharField(
        max_length=255, verbose_name=_('Formula`s parameter description'))
    physical_unit = models.ForeignKey(
        PhysicalUnit, verbose_name="Unit", on_delete=models.PROTECT)
    default_measure = models.ForeignKey(
        Measure, verbose_name="Default measure", on_delete=models.PROTECT)

    def __str__(self):
        return f'ID {self.pk} - {self.description} (Physical Unit:{self.physical_unit},Measure:{self.default_measure})'

    class Meta:
        verbose_name = "Formula`s parameter"


class Formula(models.Model):
    """ Формула """
    name = models.CharField(max_length=255, verbose_name=_('Name'))
    description = models.CharField(
        max_length=255, verbose_name=_('Description'))
    parameters = models.ManyToManyField(
        FormulaParameter, verbose_name="Parameters in formula")
    expression = models.TextField(verbose_name="Formula view expression")
    calculation_expression = models.TextField(
        verbose_name="Formula calculation expression", null=True, blank=True)
    views_counter = models.IntegerField(
        verbose_name="Views counter", default=0)
    is_popular = models.BooleanField(default=False)
    related = models.ManyToManyField("self", verbose_name="Related formulas", help_text="Using for swipe",
                                     blank=True)

    def __str__(self):
        return f'{self.name}'


class Experiment(mixins.CreatedAtMixin, mixins.UpdatedAtMixin, mixins.DescriptionPreviewMixin):
    """Сущность эксперимента (МЕТОДА по терминологии заказчика)"""
    name = models.CharField(max_length=255, verbose_name=_('Method name'))
    company = models.CharField(max_length=255, verbose_name=_(
        'Company name'), null=True, blank=True)
    description = models.TextField(verbose_name=_(
        'Description'), null=True, blank=True)
    is_popular = models.BooleanField(
        default=False, verbose_name=_('Is popular'))
    is_published = models.BooleanField(
        default=False, verbose_name=_('Is published'))

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = "Method"


class ExperimentStage(mixins.CreatedAtMixin, mixins.UpdatedAtMixin, mixins.DescriptionPreviewMixin):
    """Сущность этапа эксперимента (МЕТОДА по терминологии заказчика)"""
    experiment = models.ForeignKey(Experiment, on_delete=models.CASCADE, verbose_name=_('Experiment'),
                                   related_name="stages")
    name = models.CharField(max_length=255, verbose_name=_('Name'))
    description = models.TextField(verbose_name=_(
        'Description'), null=True, blank=True)
    position = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['position']
        verbose_name = "Stage"


class StageStep(mixins.CreatedAtMixin, mixins.UpdatedAtMixin, mixins.DescriptionPreviewMixin):
    """Сущность шага на этапе эксперимента (МЕТОДА по терминологии заказчика)"""
    stage = models.ForeignKey(ExperimentStage, on_delete=models.CASCADE, verbose_name=_(
        'Stage'), related_name="steps")
    name = models.CharField(max_length=255, verbose_name=_('Name'))
    description = models.TextField(verbose_name=_('Description'))
    help_text = models.TextField(verbose_name=_(
        'Help text'), blank=True, null=True)
    position = models.PositiveSmallIntegerField(null=True)
    can_create_note = models.BooleanField(
        default=False, verbose_name=_('Notes tool'))
    can_create_timer = models.BooleanField(
        default=False, verbose_name=_('Timer tool'))
    timer_default_duration = models.DurationField(verbose_name=_('Timer default duration'),
                                                  help_text="Used only when timer tool is checked", blank=True, null=True)
    formula = models.ForeignKey(Formula, verbose_name=_(
        'Formula'), on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['position']
        verbose_name = "Step"

    @admin.display(boolean=None, ordering='stage__experiment', description="Method")
    def experiment(self):
        return f'{self.stage.experiment}'


class ActiveExperiment(models.Model):
    """ Активный эксперимент пользователя"""
    user = models.ForeignKey(UserModel, on_delete=models.CASCADE)
    experiment = models.ForeignKey(Experiment, on_delete=models.CASCADE, related_name="active_experiments")
    stage = models.ForeignKey(ExperimentStage, on_delete=models.CASCADE, blank=True, null=True)
    step = models.ForeignKey(StageStep, on_delete=models.CASCADE, blank=True, null=True)


def float_to_str(f):
    "Конвертирует строку из scientific notation"
    float_string = repr(f)
    if 'e' in float_string:  # detect scientific notation
        digits, exp = float_string.split('e')
        digits = digits.replace('.', '').replace('-', '')
        exp = int(exp)
        # minus 1 for decimal point in the sci notation
        zero_padding = '0' * (abs(int(exp)) - 1)
        sign = '-' if f < 0 else ''
        if exp > 0:
            float_string = '{}{}{}.0'.format(sign, digits, zero_padding)
        else:
            float_string = '{}0.{}{}'.format(sign, zero_padding, digits)
    return float_string

# -*- coding: utf -*-
from django.contrib import admin
from django.db import models
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _


class CreatedAtMixin(models.Model):
    """Поле с датой создания."""

    created_at = models.DateTimeField(_('Дата создания'), auto_now_add=True)

    class Meta:
        abstract = True


class UpdatedAtMixin(models.Model):
    """Дата обновления. При создании подставляются всегда текущие дата/время."""

    updated_at = models.DateTimeField(_('Дата и время обновления'), auto_now_add=True, db_index=True)

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        """Если модель фактически меняется, то обновляем значение поля updated_at."""
        self.updated_at = now()
        if update_fields and 'updated_at' not in update_fields:
            update_fields = tuple(update_fields) + ('updated_at',)
        super().save(force_insert, force_update, using, update_fields)


class DescriptionPreviewMixin(models.Model):
    """preview description для админки list_display"""

    class Meta:
        abstract = True

    @admin.display(boolean=None, ordering='description', description="Description preview")
    def description_preview(self):
        return self.description[:50]

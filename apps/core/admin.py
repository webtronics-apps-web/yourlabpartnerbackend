from django.contrib import admin

from apps.core.forms import ExperimentAdminForm, FormulaAdminForm
from apps.core.models import (StageStep, ExperimentStage,
                              Experiment, Feedback, Measure,
                              PhysicalUnit, Formula, FormulaParameter)
import nested_admin


class StepInline(nested_admin.NestedStackedInline):
    model = StageStep
    extra = 0
    sortable_field_name = "position"


class StageInline(nested_admin.NestedStackedInline):
    model = ExperimentStage
    extra = 0
    sortable_field_name = "position"
    inlines = [
        StepInline,
    ]


@admin.register(Experiment)
class ExperimentAdmin(nested_admin.NestedModelAdmin):
    list_display = ['pk', 'is_published', 'name', 'company', 'description_preview',
                    'is_popular', 'created_at', 'updated_at']
    list_filter = ('is_published', 'company', 'is_popular')
    list_editable = ['is_published']
    list_display_links = ['pk', 'name', 'company', 'description_preview']
    form = ExperimentAdminForm
    search_fields = ['name', ]
    save_as = True
    inlines = [
        StageInline,
    ]


@admin.register(ExperimentStage)
class ExperimentStageAdmin(admin.ModelAdmin):
    list_display = ['pk', 'experiment', 'name', 'description_preview', 'position', 'created_at', 'updated_at']
    list_filter = ('experiment', )
    list_display_links = ['pk', 'experiment', 'name', 'description_preview']
    search_fields = ['name', 'experiment']
    save_as = True


@admin.register(StageStep)
class StageStepAdmin(admin.ModelAdmin):
    list_display = ['pk', 'experiment', 'stage', 'name', 'description_preview', 'position',
                    'can_create_note', 'can_create_timer', 'formula', 'created_at', 'updated_at']
    list_filter = ('stage', )
    list_display_links = ['pk', 'experiment', 'stage', 'name', 'description_preview']
    search_fields = ['name', 'stage']
    save_as = True


@admin.register(Feedback)
class FeedbackAdmin(admin.ModelAdmin):
    list_display = ['pk', 'type', 'status', 'username', 'user', 'name',
                    'message_preview', 'source', 'created_at', 'updated_at']
    list_filter = ('type', 'status')
    list_editable = ['status']
    list_display_links = ['pk', 'type', 'created_at', 'updated_at', 'name', 'message_preview']


@admin.register(Measure)
class MeasureAdmin(admin.ModelAdmin):
    list_display = ['pk', 'name', 'symbol', 'parent', 'multiplier_to_main']
    list_filter = ('parent', )
    list_display_links = ['pk', 'name', 'symbol']
    save_as = True

    def has_module_permission(self, request):
        return False


@admin.register(PhysicalUnit)
class PhysicalUnitAdmin(admin.ModelAdmin):
    list_display = ['pk', 'name', 'symbol', 'default_measure']
    list_filter = ('default_measure',)
    list_display_links = ['pk', 'name', 'symbol']
    save_as = True

    def has_module_permission(self, request):
        return False


class FormulaParameterInline(admin.TabularInline):
    model = Formula.parameters.through
    extra = 0
    verbose_name = "Parameter in formula"
    verbose_name_plural = "Parameters in formula"
    show_change_link = False


@admin.register(Formula)
class FormulaAdmin(admin.ModelAdmin):
    list_display = ['pk', 'name', 'expression', 'is_popular', 'views_counter']
    list_filter = ('is_popular', )
    list_display_links = ['pk', 'name', ]
    change_form_template = "core/admin/formula_change_form.html"
    exclude = ("parameters", )
    search_fields = ['name']
    save_as = True
    inlines = [
        FormulaParameterInline,
    ]

    def has_module_permission(self, request):
        return False


@admin.register(FormulaParameter)
class FormulaParameterAdmin(admin.ModelAdmin):
    list_display = ['pk', 'description', 'physical_unit', 'default_measure']
    list_filter = ['physical_unit']
    search_fields = ['description']
    form = FormulaAdminForm
    save_as = True

    def has_module_permission(self, request):
        return False

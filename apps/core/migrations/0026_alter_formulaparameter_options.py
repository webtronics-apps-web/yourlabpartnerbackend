# Generated by Django 4.0 on 2022-01-17 09:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0025_alter_experiment_options_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='formulaparameter',
            options={'verbose_name': 'Formula`s parameter'},
        ),
    ]

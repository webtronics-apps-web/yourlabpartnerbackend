# Generated by Django 4.0 on 2022-01-11 13:59

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0015_timer'),
    ]

    operations = [
        migrations.AlterField(
            model_name='timer',
            name='running_time',
            field=models.DurationField(default=datetime.timedelta(0), help_text='Сколько уже натикало'),
        ),
    ]

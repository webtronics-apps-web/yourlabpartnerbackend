# Generated by Django 4.0 on 2021-12-30 13:55

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Experiment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('updated_at', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Дата и время обновления')),
                ('name', models.CharField(max_length=255, verbose_name='Experiment')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]

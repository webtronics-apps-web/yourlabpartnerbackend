# Generated by Django 4.0 on 2022-01-15 18:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0022_remove_stagestep_experiment_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='stagestep',
            options={'ordering': ['position']},
        ),
        migrations.AlterField(
            model_name='experimentstage',
            name='position',
            field=models.PositiveSmallIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='stagestep',
            name='position',
            field=models.PositiveSmallIntegerField(null=True),
        ),
    ]

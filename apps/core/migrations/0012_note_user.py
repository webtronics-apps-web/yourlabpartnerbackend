# Generated by Django 4.0 on 2022-01-10 13:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0002_customuser_last_activity'),
        ('core', '0011_delete_notetags'),
    ]

    operations = [
        migrations.AddField(
            model_name='note',
            name='user',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='user.customuser'),
            preserve_default=False,
        ),
    ]

# Generated by Django 4.0 on 2022-03-10 11:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0008_remove_customuser_active_experiments'),
        ('core', '0038_alter_noteimage_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActiveExperiment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('experiment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.experiment')),
                ('stage', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='core.experimentstage')),
                ('step', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='core.stagestep')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user.customuser')),
            ],
        ),
    ]

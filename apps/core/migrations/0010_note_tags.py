# Generated by Django 4.0 on 2022-01-10 13:13

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_alter_noteimage_note'),
    ]

    operations = [
        migrations.AddField(
            model_name='note',
            name='tags',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=100), default=list, size=None),
        ),
    ]

from django.core.management.base import BaseCommand
from firebase_admin.messaging import Message, Notification

from apps.notifications.models import CustomFCMDevice


class Command(BaseCommand):
    help = 'Добавляет во все проекты регионы и проекты, согласно снятым позициям'

    def handle(self, *args, **options):
        devices = CustomFCMDevice.objects.all()
        results = devices.send_message(Message(
            notification=Notification(
                title="Test notification2", body="Test bulk notification"
            ),
            data={
                'message_id': "1",
                'click_action': 'FLUTTER_NOTIFICATION_CLICK',
                'navigate_to': "formulae/1",
            },))
        print(results.response)

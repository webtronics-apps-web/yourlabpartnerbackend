from .models import (EventsTypes, CustomFCMDevice, ReminderPeriodicPush, NewsPush, PushTypes)
from rest_framework.response import Response
from rest_framework import status


class EventsManager():

    def raise_stat_event(self, request, event_id, fcm_token) -> Response:
        if event_id == EventsTypes.ON_APP_START:
            return self.__on_app_start_event(request=request, fcm_token=fcm_token)
        else:
            return self.__on_events_without_fcm(request, event_id)

    def __on_events_without_fcm(self, request, event_id):
        self.__auth_check(request)
        self.__update_event_stats(request, event_id)
        modal = request.user.get_rate_modal(event_id)
        return self.__get_modal_response(modal)

    def raise_push_click_event(self, notification_id, notification_type) -> Response:
        notification_model = NewsPush if notification_type == PushTypes.NEWS.value else ReminderPeriodicPush
        notification = notification_model.objects.get(pk=notification_id)
        notification.opened_notifications_counter += 1
        notification.save(update_fields=['opened_notifications_counter'])
        return Response("ok", status=status.HTTP_200_OK)

    def __auth_check(self, request):
        if not bool(request.user and request.user.is_authenticated):
            raise Exception('User is not authenticated!')

    def __get_modal_response(self, modal):
        if not modal:
            return Response({"show_rate": False}, status=status.HTTP_200_OK)
        modal.sended_modals_counter += 1
        modal.save(update_fields=['sended_modals_counter'])
        return Response({"show_rate": True,
                         "modal_title": modal.title,
                         "modal_message": modal.content,
                         "modal_id": modal.pk}, status=status.HTTP_200_OK)

    def __update_event_stats(self, request, event_id):
        match event_id:
            case EventsTypes.ON_FIRST_FORMULA_RESULT:
                request.user.update_last_activity()
                request.user.calculated_formulas_counter += 1
                request.user.save(update_fields=['calculated_formulas_counter'])
            case EventsTypes.ON_NOTE_SAVE:
                request.user.update_last_activity()
                request.user.saved_notes_counter += 1
                request.user.save(update_fields=['saved_notes_counter'])
            case EventsTypes.ON_TIMER_FINISH:
                request.user.update_last_activity()
                request.user.timer_finished_counter += 1
                request.user.save(update_fields=['timer_finished_counter'])

    def __on_app_start_event(self, request, fcm_token):
        if not fcm_token:
            raise Exception("FCM TOKEN WAS NOT PROVIDED")
        device, created = CustomFCMDevice.objects.select_related(
            'user').get_or_create(registration_id=fcm_token, device_id=fcm_token)
        device.app_start_counter += 1
        device.save(update_fields=['app_start_counter', 'user'])
        if request.user.is_authenticated:
            request.user.update_last_activity()
            device.user = request.user
        modal = device.get_rate_modal()
        return self.__get_modal_response(modal)

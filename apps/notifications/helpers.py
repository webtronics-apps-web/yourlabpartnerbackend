def toFixed(numObj, digits=0):
    """Форматирует число в фиксированное количество знаков после запятой"""
    return f"{numObj:.{digits}f}"

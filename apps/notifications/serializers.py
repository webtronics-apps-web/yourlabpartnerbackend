
from rest_framework.serializers import ModelSerializer, Serializer, IntegerField, CharField
from fcm_django.api.rest_framework import UniqueRegistrationSerializerMixin

from .models import CustomFCMDevice


class CustomDeviceSerializerMixin(ModelSerializer):
    class Meta:
        fields = (
            "id",
            "name",
            "registration_id",
            "device_id",
            "active",
            "date_created",
            "type",
            'app_start_counter'
        )
        read_only_fields = ("date_created",)

        extra_kwargs = {"active": {"default": True}}


class CustomFCMDeviceSerializer(ModelSerializer, UniqueRegistrationSerializerMixin):
    class Meta(CustomDeviceSerializerMixin.Meta):
        model = CustomFCMDevice

        extra_kwargs = {"id": {"read_only": True, "required": False}}
        extra_kwargs.update(CustomDeviceSerializerMixin.Meta.extra_kwargs)


class RaiseStatEventSerializer(Serializer):
    event_id = IntegerField()
    fcm_token = CharField(required=False)


class RaisePushEventSerializer(Serializer):
    notification_id = IntegerField()
    notification_type = IntegerField()


class FcmTokenSerializer(Serializer):
    fcm_token = CharField()


class RaiseModalAcceptedSerializer(Serializer):
    modal_id = IntegerField()

import json
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.http import HttpResponseRedirect

from apps.core.models import Experiment, Formula
from .models import ModalWindow, ReminderPeriodicPush, CustomFCMDevice, NewsPush
from django_celery_beat.models import PeriodicTask, IntervalSchedule
from django.utils import timezone
# from .forms import CustomUserAdminForm

UserModel = get_user_model()


@admin.register(CustomFCMDevice)
class CustomFCMDeviceAdmin(admin.ModelAdmin):
    list_display = ['pk', 'registration_id', 'user', 'app_start_counter', 'created_at']
    list_display_links = ['pk', 'registration_id']
    list_filter = ('user',)
    readonly_fields = ['created_at', ]
    exclude = ('type',)


@admin.register(ModalWindow)
class ModalWindowAdmin(admin.ModelAdmin):
    list_display = ['pk', 'title', 'is_enabled', 'event', 'event_counter',
                    'sended_modals_counter', 'opened_target_modals_counter', 'success_rate']
    list_display_links = ['pk', 'title']
    list_filter = ('is_enabled', 'event')
    search_fields = ['title', ]
    readonly_fields = ('success_rate', 'sended_modals_counter', 'opened_target_modals_counter')
    fields = ['title', 'is_enabled', 'content', 'window_type',
              'event',
              'event_counter', 'sended_modals_counter',
              'opened_target_modals_counter', 'success_rate']


@admin.register(ReminderPeriodicPush)
class ReminderPeriodicPushAdmin(admin.ModelAdmin):
    list_display = ['pk', 'title', 'is_enabled', 'remind_type',
                    'sended_notifications_counter',
                    'opened_notifications_counter',
                    'target_screen', 'success_rate'
                    ]
    list_display_links = ['pk', 'title']
    search_fields = ['title', ]
    list_filter = ('is_enabled', 'remind_type')
    change_form_template = "notifications/admin/reminderpush_change_form.html"
    readonly_fields = ('success_rate', 'sended_notifications_counter', 'opened_notifications_counter')
    fields = ['title', 'is_enabled', 'message',
              'target_screen',
              'target_screen_id', 'check_frequency', 'condition_period',
              'sended_notifications_counter',
              'opened_notifications_counter',
              'success_rate']

    def add_view(self, request, form_url='', extra_context=None):
        extra_context = extra_context or {}

        extra_context['formulas'] = list(Formula.objects.all().values('pk', 'name'))
        extra_context['methods'] = list(Experiment.objects.all().values('pk', 'name'))
        return super(ReminderPeriodicPushAdmin, self).add_view(request, form_url=form_url,
                                                               extra_context=extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}

        extra_context['formulas'] = list(Formula.objects.all().values('pk', 'name'))
        extra_context['methods'] = list(Experiment.objects.all().values('pk', 'name'))
        return super(ReminderPeriodicPushAdmin, self).change_view(request, object_id, form_url=form_url,
                                                                  extra_context=extra_context)

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        if not change:
            start_time = timezone.now().replace(hour=12, minute=00)
            obj.task = PeriodicTask.objects.create(name=f'Reminder task: {obj.pk}',
                                                   task='send_periodic_push',
                                                   interval=IntervalSchedule.objects.get_or_create(
                                                       every=obj.check_frequency, period='days')[0],
                                                   args=json.dumps([obj.pk]),
                                                   start_time=start_time,)
            obj.save(update_fields=['task'])
        else:
            obj.task.interval = IntervalSchedule.objects.get_or_create(
                every=obj.check_frequency, period='days')[0]
            obj.task.save()

    def delete_queryset(self, request, queryset):
        for push in queryset:
            if push.task:
                push.task.delete()
        super().delete_queryset(request, queryset)

    def delete_model(self, request, obj):
        if obj.task:
            obj.task.delete()
        super().delete_model(request, obj)

    def response_change(self, request, obj):
        if "_save_and_send_test_to_me" in request.POST:
            obj.save()
            if request.user.customfcmdevice_set.count() < 1:
                self.message_user(request, "There is no devices attached to current user!")
                return HttpResponseRedirect(".")
            obj.send_notification_to_user(request.user)
            self.message_user(request, "Push message was send!")
            return HttpResponseRedirect(".")
        elif "_save_and_send_to_all" in request.POST:
            obj.save()
            obj.send_notifications()
            self.message_user(request, "Push messages was send to all devices!")
            return HttpResponseRedirect(".")

        return super().response_change(request, obj)


@admin.register(NewsPush)
class NewsPushAdmin(admin.ModelAdmin):
    list_display = ['pk', 'title', 'sended_notifications_counter',
                    'opened_notifications_counter',
                    'target_screen',  'last_send', 'success_rate']
    list_display_links = ['pk', 'title']
    search_fields = ['title', ]
    readonly_fields = ('success_rate', 'sended_notifications_counter',
                       'opened_notifications_counter', 'last_send')
    fields = ['title', 'message',
              'target_screen',
              'target_screen_id',
              'last_send',
              'sended_notifications_counter',
              'opened_notifications_counter',
              'success_rate', ]
    change_form_template = "notifications/admin/newspush_change_form.html"

    def add_view(self, request, form_url='', extra_context=None):
        extra_context = extra_context or {}

        extra_context['formulas'] = list(Formula.objects.all().values('pk', 'name'))
        extra_context['methods'] = list(Experiment.objects.all().values('pk', 'name'))
        return super(NewsPushAdmin, self).add_view(request, form_url=form_url,
                                                   extra_context=extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}

        extra_context['formulas'] = list(Formula.objects.all().values('pk', 'name'))
        extra_context['methods'] = list(Experiment.objects.all().values('pk', 'name'))
        return super(NewsPushAdmin, self).change_view(request, object_id, form_url=form_url,
                                                      extra_context=extra_context)

    def response_change(self, request, obj):
        if "_save_and_send_test_to_me" in request.POST:
            obj.save()
            if request.user.customfcmdevice_set.count() < 1:
                self.message_user(request, "There is no devices attached to current user!")
                return HttpResponseRedirect(".")
            obj.send_notification_to_user(request.user)
            self.message_user(request, "Push message was send!")
            return HttpResponseRedirect(".")

        elif "_save_and_send_to_all" in request.POST:
            obj.save()
            obj.send_notifications()
            self.message_user(request, "Push messages was send to all devices!")
            return HttpResponseRedirect(".")

        return super().response_change(request, obj)

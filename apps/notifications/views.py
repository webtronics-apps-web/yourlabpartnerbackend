from rest_framework import status
from rest_framework.response import Response
from drf_spectacular.utils import extend_schema_view, extend_schema
from fcm_django.api.rest_framework import (DeviceViewSetMixin, ModelViewSet)
from .models import CustomFCMDevice, ModalWindow
from .serializers import (CustomFCMDeviceSerializer, FcmTokenSerializer,
                          RaiseStatEventSerializer, RaisePushEventSerializer,
                          RaiseModalAcceptedSerializer)
from rest_framework.decorators import action
from rest_framework.viewsets import ViewSet
from .managers import EventsManager


class CustomFCMDeviceViewSet(DeviceViewSetMixin, ModelViewSet):
    queryset = CustomFCMDevice.objects.order_by("-id")
    serializer_class = CustomFCMDeviceSerializer


@ extend_schema_view(
    raise_stat_event=extend_schema(request=RaiseStatEventSerializer,
                                   description='Вызвать событие для статистики + модальные окна'),
    raise_push_click_event=extend_schema(request=RaisePushEventSerializer,
                                         description='Вызвать событие на клик по пушу'),
    raise_modal_accepted=extend_schema(request=RaiseModalAcceptedSerializer,
                                       description='Вызвать событие на клик по пушу'),
    set_user_fcm_token=extend_schema(request=FcmTokenSerializer,
                                     description='Устанавливает переданный токен авторизованному пользователю'),
)
class EventsApiViewSet(ViewSet):
    @action(detail=False, methods=['post'])
    def raise_stat_event(self, request):
        serializer = RaiseStatEventSerializer(data=request.data)
        if serializer.is_valid():
            event_id, fcm_token = serializer.validated_data['event_id'], serializer.validated_data.get('fcm_token')
            manager = EventsManager()
            return manager.raise_stat_event(request, event_id=int(event_id), fcm_token=fcm_token)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def raise_push_click_event(self, request):
        serializer = RaisePushEventSerializer(data=request.data)
        if serializer.is_valid():
            notif_id, notif_type = serializer.validated_data['notification_id'], serializer.validated_data.get(
                'notification_type')
            manager = EventsManager()
            return manager.raise_push_click_event(notification_id=notif_id, notification_type=notif_type)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def raise_modal_accepted(self, request):
        self.__check_auth(request)
        serializer = RaiseModalAcceptedSerializer(data=request.data)
        if serializer.is_valid():
            modal_id = serializer.validated_data['modal_id']
            ModalWindow.on_modal_accept(modal_id)
            request.user.is_rated_app = True
            request.user.save(update_fields=['is_rated_app'])
            return Response("ok", status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
            
    @action(detail=False, methods=['post'])
    def do_not_show_rate_modal(self, request):
        self.__check_auth(request)
        request.user.is_show_rate_modal = False
        request.user.save(update_fields=['is_show_rate_modal'])
        return Response("ok", status=status.HTTP_200_OK)

    @action(detail=False, methods=['post'])
    def set_user_fcm_token(self, request):
        self.__check_auth(request)
        serializer = FcmTokenSerializer(data=request.data)
        if serializer.is_valid():
            fcm_token = serializer.validated_data['fcm_token']
            device = CustomFCMDevice.objects.get_or_create(registration_id=fcm_token, device_id=fcm_token)[0]
            device.user = request.user
            device.save(update_fields=['user'])
            return Response("ok",
                            status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
        
    def __check_auth(self, request):
        if not request.user or not request.user.is_authenticated:
            raise Exception("User is not authentificated!")

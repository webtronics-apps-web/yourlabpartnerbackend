import datetime
from enum import Enum
from django.db import models
from django.db.models import F
from django.utils import timezone
from django.db.models.functions import Mod
from fcm_django.models import AbstractFCMDevice
from django.core.validators import MinValueValidator
from django.utils.translation import gettext_lazy as _
from firebase_admin.messaging import Message, Notification
from django_celery_beat.models import PeriodicTask

from apps.notifications.helpers import toFixed


class CustomFCMDevice(AbstractFCMDevice):
    app_start_counter = models.PositiveIntegerField(verbose_name="Application started count", default=0)
    created_at = models.DateTimeField(verbose_name="Created at", auto_now_add=True)

    class Meta:
        verbose_name = _("Device")
        verbose_name_plural = _("Devices")

    def get_rate_modal(self):
        if not self.user or not self.user.is_show_rate_modal:
            return None
        return ModalWindow.get_modal_to_show(self.app_start_counter, EventsTypes.ON_APP_START)


class PushTypes(Enum):
    NEWS = 1
    REMINDER = 2


class PushNotification(models.Model):
    title = models.CharField("Title", max_length=30)
    message = models.CharField("Text", max_length=40)
    sended_notifications_counter = models.PositiveIntegerField(verbose_name="Sended notifications counter", default=0)
    opened_notifications_counter = models.PositiveIntegerField(verbose_name="Accepted notifications counter", default=0)

    class TargetScreens(models.TextChoices):
        FORMULAS = 'FRMLS', _('Formulaes Screen')
        FORMULA_ID = 'FRML_ID', _('Selected Formulae')
        METHODS = 'MTHDS', _('Methods Screen')
        METHOD_ID = 'MTHD_ID', _('Selected Method')
        TIMERS = 'TIMERS', _('Timers Screen')
        NOTES = 'NOTES', _('Notes Screen')
        EMPTY = 'EMPT', _('No target screen')

    target_screen = models.CharField(verbose_name="Target screen", max_length=7,
                                     choices=TargetScreens.choices, default=TargetScreens.NOTES)
    target_screen_id = models.PositiveIntegerField(verbose_name="Select value for target screen", null=True, blank=True)
    last_send = models.DateTimeField(verbose_name="Last send was", blank=True, null=True)

    @property
    def success_rate(self):
        if self.sended_notifications_counter == 0:
            return 0
        percentage = self.opened_notifications_counter / self.sended_notifications_counter * 100
        return f'{toFixed(percentage)}%'

    def main_send_notifications(self, qs=None, notification_type: int = None):
        """Отправляет уведомление и сохраняет статич.данные"""
        devices = qs or self.__get_queryset_for_sending()
        notif_type = notification_type or PushTypes.NEWS
        message_data = {
            'notification_id': str(self.pk),
            'notification_type': str(notif_type.value),
            'click_action': 'FLUTTER_NOTIFICATION_CLICK',
            'navigate_to': self.__get_target_screen_url()}
        results = devices.send_message(Message(
            notification=Notification(title=self.title, body=self.message),
            data=message_data))
        self.sended_notifications_counter += results.response.success_count
        self.last_send = timezone.now()
        self.save(update_fields=['sended_notifications_counter', 'last_send'])

    def main_send_notification_to_user(self, user, notification_type: int = None):
        """Отправляет уведомление и сохраняет статич.данные"""
        devices = CustomFCMDevice.objects.filter(user=user)
        notif_type = notification_type or PushTypes.NEWS
        message_data = {
            'notification_id': str(self.pk),
            'notification_type': str(notif_type.value),
            'click_action': 'FLUTTER_NOTIFICATION_CLICK',
            'navigate_to': self.__get_target_screen_url()}
        results = devices.send_message(Message(
            notification=Notification(title=self.title, body=self.message),
            data=message_data))
        self.sended_notifications_counter += results.response.success_count
        self.last_send = timezone.now()
        self.save(update_fields=['sended_notifications_counter', 'last_send'])

    def __get_queryset_for_sending(self):
        return CustomFCMDevice.objects.all()

    def __get_target_screen_url(self):
        match self.target_screen:
            case self.TargetScreens.FORMULAS:
                return 'formulae'
            case self.TargetScreens.FORMULA_ID:
                return f'formulae/{self.target_screen_id}'
            case self.TargetScreens.METHODS:
                return 'methods'
            case self.TargetScreens.METHOD_ID:
                return f'methods/{self.target_screen_id}'
            case self.TargetScreens.TIMERS:
                return 'timers'
            case self.TargetScreens.NOTES:
                return 'notes'
            case _:
                return ''

    class Meta:
        abstract = True

    def __str__(self):
        return self.title


class NewsPush(PushNotification):

    def send_notifications(self):
        self.main_send_notifications()

    def send_notification_to_user(self, user):
        self.main_send_notification_to_user(user=user)

    class Meta:
        verbose_name = "News push notification"
        verbose_name_plural = "News push notifications"


class ReminderPeriodicPush(PushNotification):
    is_enabled = models.BooleanField(verbose_name="Is enabled", default=False)
    # С какой периодичностью проверять условие (запускать задачу в Celery)
    check_frequency = models.PositiveSmallIntegerField(verbose_name="Start task interval", default=3,
                                                       validators=[MinValueValidator(1)],
                                                       help_text="Interval in days for find and send notifications \
                                                       to users who meets the requirement condition")
    # Период отсутвия активности/период прошедший с момента установки
    condition_period = models.PositiveSmallIntegerField(verbose_name="Condition interval (days)", help_text="Condition (days) to select users. \
                                                        For example, if type is 'Remind to open app'  \
                                                        and 'Condition interval' is set to 14, \
                                                        Reminder will be sent to all users,  \
                                                        who wasn`t active for >= 14 days. \
                                                        If type is 'Remind to register app', \
                                                        reminder will be sent to all users,  \
                                                        who installed app >= 14 days ago, but did not register",
                                                        default=14, validators=[MinValueValidator(1)])
    task = models.ForeignKey(PeriodicTask, on_delete=models.SET_NULL, blank=True, null=True)

    class RemindTypes(models.TextChoices):
        REMIND_TO_REGISTER = 'REG', _('Remind to register app')
        REMIND_TO_OPEN_APP = 'ACT', _('Remind to open app')

    remind_type = models.CharField(verbose_name="Notification type", max_length=3,
                                   choices=RemindTypes.choices, default=RemindTypes.REMIND_TO_REGISTER)

    def send_notifications(self):
        self.main_send_notifications(qs=self.__get_queryset_for_sending(), notification_type=PushTypes.REMINDER)

    def send_notification_to_user(self, user):
        self.main_send_notification_to_user(notification_type=PushTypes.REMINDER, user=user)

    def __get_queryset_for_sending(self):
        date_condition = timezone.now() - datetime.timedelta(days=self.condition_period)
        if self.remind_type == self.RemindTypes.REMIND_TO_REGISTER:
            return CustomFCMDevice.objects.filter(user=None, date_created__lte=date_condition)
        return CustomFCMDevice.objects.exclude(user=None).filter(user__last_activity__lte=date_condition)

    class Meta:
        verbose_name = "Periodic push-reminder"
        verbose_name_plural = "Periodic push-reminders"


class EventsTypes(models.IntegerChoices):
    ON_NOTE_SAVE = 1, _('On note save')
    ON_TIMER_FINISH = 2, _('On timer message click')
    ON_FIRST_FORMULA_RESULT = 3, _('On first time formula result receive')
    ON_APP_START = 4, _('On application start')


class ModalWindow(models.Model):
    title = models.CharField("Title", max_length=35)
    content = models.CharField("Text", max_length=60)
    is_enabled = models.BooleanField(verbose_name="Is enabled", default=False)

    event = models.IntegerField(verbose_name="Action(event) to check",
                                choices=EventsTypes.choices, default=EventsTypes.ON_APP_START)

    event_counter = models.PositiveSmallIntegerField(verbose_name="Action(event) interval", default=5,
                                                     validators=[MinValueValidator(1)])

    sended_modals_counter = models.PositiveIntegerField(verbose_name="Modals showed", default=0)
    opened_target_modals_counter = models.PositiveIntegerField(verbose_name="Successed target actions",
                                                               help_text="Count of users, who opened store to rate app \
                                                               (if modal type is 'Rate APP')",
                                                               default=0)

    class WindowTypes(models.TextChoices):
        RATE_APP = 'RATE', _('Rate application')

    window_type = models.CharField(verbose_name="Window type", max_length=4,
                                   choices=WindowTypes.choices, default=WindowTypes.RATE_APP)

    @property
    def success_rate(self):
        if self.sended_modals_counter == 0:
            return 0
        percentage = self.opened_target_modals_counter / self.sended_modals_counter * 100
        return f'{toFixed(percentage)}%'

    @staticmethod
    def on_modal_accept(modal_id: int):
        modal = ModalWindow.objects.get(pk=modal_id)
        modal.opened_target_modals_counter += 1
        modal.save(update_fields=['opened_target_modals_counter'])

    @staticmethod
    def get_modal_to_show(event_counter, event_id):
        return ModalWindow.objects.annotate(
            is_not_need_to_show=Mod(
                event_counter, F('event_counter'))
        ).filter(is_enabled=True, event=event_id, is_not_need_to_show=0).last()

    class Meta:
        verbose_name = _("Modal window")
        verbose_name_plural = _("Modal windows")

    def __str__(self):
        return self.title

from multiprocessing import get_logger
from .models import ReminderPeriodicPush
from your_lab_partner.celery import app
logger = get_logger()


@app.task(name="send_periodic_push")
def send_periodic_push(push_id):
    logger.debug(f"Начинаю отправку сообщение по пушу с id {push_id}")
    push = ReminderPeriodicPush.objects.get(pk=push_id)
    push.send_notifications()
    logger.debug(f"Закончил отправку сообщение по пушу с id {push_id}")
